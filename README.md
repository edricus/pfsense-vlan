# pfsense-vlan
Un script pour automatiser la création de VLANs sur pfsense.  
Il modifie le fichier **config.xml** préalablement exporté.  
Un backup de ce fichier sera crée au moment de l'execution.  

## Exportation du fichier config.xml
#### Dans l'interface web
1) Aller dans "Diagnostic > Backup & Restore"  
2) Cliquer sur "Download configuration as XML"  

## Utilisation du script
#### En ligne de commande
```console
./pfsense.sh </path/to/config.xml>
```
Le script effectura un backup et modifiera le fichier en entrée.  
Après la modification, importer de nouveau le fichier config.

## Importation du fichier config.xml modifié  
<h4> Dans l'interface web </h4>

1) Aller dans **Diagnostic > Backup & Restore**  
2) Cliquer sur **Browse** et selectionner le fichier config.xml  
3) Cliquer sur **Restore configuration**  


## Limitations

- Ce script va se fermer s'il rencontre un tag déjà présent dans le fichier de base.  
- Limitation de pfsense: l'importation redémarre entièrement le serveur.  

## Pistes d'amélioration

- Automatiser toute la config de pfsense (autre projet/renommage)  
- Inclure le service DHCP  
- Inclure des plugins  
- Attriber des IPs en fonction du tag, ex: VLAN 99 = 192.168.0.99 (en demandant avant)  
- Choix des noms des interfaces / laisser OPT
- Vérification plus rigoureuses des entrées de l'utilisateur  
