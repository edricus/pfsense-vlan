#!/bin/bash
# readlink: chemin relatif à absolu
XML=$(readlink -f "$1")

if [[ $# -ne 1 ]]; then
  echo "Utilisation:    $0 </chemin/du/fichier/config.xml>"
  exit 
fi

tmpvlans="/tmp/tempvlan"
tmpports="/tmp/tempports"
tmpdhcp="/tmp/tempdhcp"
SERVICESLIST=$(mktemp)

# Supprime les fichiers temporaires si Ctrl+C ou EXIT
trap "rm -f $tmpvlans $tmpports $tmpdhcp $SERVICESLIST \
	&& printf 'Fichiers temporaires supprimés\n' \
	&& exit" SIGINT EXIT RETURN

#################
## Préparation ##
#################
if [[ -f "$XML.bak" ]]; then
  echo "Un backup existe déjà, il ne sera pas écrasé"
else
  cp $XML $XML.bak	
  echo "Backup créé : $XML.bak"
fi

LANIF=$(grep -A2 -m1 lan $XML | grep if | cut -f4 | sed -Ee 's/<|<\/|if|>//g')
WANIF=$(grep -A2 -m1 wan $XML | grep if | cut -f4 | sed -Ee 's/<|<\/|if|>//g')
LANIP=$(grep -m1 -A3 '<lan>' dhcp.xml | grep '<ipaddr>' | cut -f4 | sed -Ee 's/<|<\/|ipaddr|>//g')
#LANIP=$(ifconfig $LANIF | grep 'inet ' | cut -f2 | grep -oE "([0-9]{1,3}\.){3}[0-9]{1,3}" | sed 2d)
#WANIP=$(ifconfig $WANIF | grep 'inet ' | cut -f2 | grep -oE "([0-9]{1,3}\.){3}[0-9]{1,3}" | sed 2d)

#############
# Questions #
#############
menu() {
	ANSWER=$(
	whiptail --title "Menu principal" --backtitle "Configuration de pfsense" --menu \
		"Choisir les composants à installer ou à modifier" 13 70 4 \
			"1." "Installer des services" \
			"2." "Configurer les utilisateurs" \
			"3." "Modifier des services" \
			"4." "Quitter" \
			3>&2 2>&1 1>&3
	)
	[[ "$?" = 1 ]] && exit 1
}
services() { 
	SERVICES=$(
	whiptail --title "Choix des services" --backtitle "Configuration des services" --menu \
		"Selectionner les services à installer" 13 70 3 \
		"1." "VLANs: Créer des VLANs en masse" \
		"2." "DHCP: Activer le DHCP et configurer l'étendue" \
		"3." "Quitter" \
		3>&1 1>&2 2>&3
	)
	[[ "$?" = 1 ]] && exit 1
	
	vlans() {
		# questions
		IF=$(
		whiptail --title "Choix de l'interface" --backtitle "Configuration des VLANs" --radiolist --separate-output \
	  	  "Interface sur laquelle créer les VLANs" 8 33 2 \
				"$LANIF" "Interface LAN" ON \
				"$WANIF" "Interface WAN" OFF \
				3>&1 1>&2 2>&3 
		)
		[[ "$?" = 1 ]] && exit 1
		START=$(
		whiptail --title "Étendue de VLANs" --backtitle "Configuration des VLANs" --inputbox \
	  	  "Tag de la première VLAN" 8 60 \
				3>&1 1>&2 2>&3 
		) 
		[[ "$?" = 1 ]] && services
		END=$(
		whiptail --title "Étendue de VLANs" --backtitle "Configuration des VLANs" --inputbox \
	  	  "Tag de la dernière VLAN" 8 60 \
				3>&1 1>&2 2>&3
		) 
		[[ $? = 1 ]] && services
		INC=$(
		whiptail --title "Étendue de VLANs" --backtitle "Configuration des VLANs" --inputbox \
	  	  "L'incrémentation entre chaques VLANs" 8 60 "1" \
				3>&1 1>&2 2>&3
		)
		[[ $? = 1 ]] && services
		whiptail --title "Configuration des interfaces" --backtitle "Configuration des VLANs" --yesno --defaultno \
				"Configurer manuellement le nom des interfaces ?\nPar défaut, les interface prennent le nom 'OPT' suivi des chiffres 1,2,3..." 8 80 \
				3>&1 1>&2 2>&3
		MANUALOPT=$?
		# exec
		export IF START END INC XML MANUALOPT tmpvlans tmpports
		export -f services
		(cd src && ./vlans.sh)
	}
	
	dhcp() {
		FROMold=$(grep -m1 '<from>' $XML | cut -f5 | sed -Ee 's/<|from|>|<\///g')
	 	TOold=$(grep -m1 '<to>' $XML | cut -f5 | sed -Ee 's/<|to|>|<\///g')
		# questions
		IF="$(whiptail --title "Choix de l'interface" --backtitle "Configuration du DHCP" --radiolist --separate-output 3>&1 1>&2 2>&3 \
	    "Interface sur laquelle le DHCP va écouter" 8 33 2 \
	    "$LANIF" "Interface LAN" ON \
	    "$WANIF" "Interface WAN" OFF )"
		exitstatus=$?
		[[ "$exitstatus" = 1 ]] && services
		
		FROM="$(whiptail --title "Création de l'entendue" --backtitle "Configuration du DHCP" --inputbox 3>&1 1>&2 2>&3 \
			"Première IP attribuable" 8 45)"
		exitstatus=$?
		[[ "$exitstatus" = 1 ]] && services
		
		TO="$(whiptail --title "Création de l'entendue" --backtitle "Configuration du DHCP" --inputbox 3>&1 1>&2 2>&3 \
			"Dernière IP attribuable" 8 45)"
		exitstatus=$?
		[[ "$exitstatus" = 1 ]] && services
		
		GATEWAY="$(whiptail --title "Attribution de la passerelle" --backtitle "Configuration du DHCP" --inputbox 3>&1 1>&2 2>&3 \
			"Adresse de la passerelle" 8 45 "$LANIP")"
		exitstatus=$?
		[[ "$exitstatus" = 1 ]] && services
		
		DNS="$(whiptail --title "Serveurs DNS" --backtitle "Configuration du DHCP" --inputbox 3>&1 1>&2 2>&3 \
			"Serveurs DNS, séparés par des virgules" 8 45 "1.1.1.1,1.0.0.1")"
		exitstatus=$?
		[[ "$exitstatus" = 1 ]] && services
			
		#STATIC=0
		#while [[ $STATIC == 0 ]]
		#do STATIC=$(whiptail --title "Ajouter un client statique" --backtitle "Configuration du DHCP" --yesno 3>&1 1>&2 2>&3 \
		#	"Voulez-vous ajouter un client statique ?" 8 33)
	
		#i=0
		#if [[ $STATIC == 0 ]]; then
			#((i++))
			#echo "1" > macerror
			#while [[ $(cat macerror) == "1" ]]	
			#do
			#STATICMAC=$(whiptail --title "Adresse MAC du client statique" --backtitle "Configuration du DHCP" --inputbox 3>&1 1>&2 2>&3 \
			#	"Adresse MAC du client statique" 8 33)
			#if [[ ! "$STATICMAC" =~ "^([a-fA-F0-9]{2}:){5}[a-fA-F0-9]{2}$" ]]; then		# teste si la variable est une adresse mac
			#	whiptail --msgbox "L'adresse mac n'est pas valide, elle doit être au format '08:00:27:df:41:d8'" 8 33
			#	echo "1" > macerror
			#fi
			#done
			#STATICIP=$(whiptail --title "Adresse IP du client statique" --backtitle "Configuration du DHCP" --inputbox 3>&1 1>&2 2>&3 \
				#"Adresse IP du client statique" 8 33)		# (note: tester si l'ip est dans l'étendue déjà existante)
			#printf "$STATICMAC\n$STATICIP\n" >> "src/clients/client$i-$STATICMAC"
			#cat "src/clients/client$i-$STATICMAC"
		#fi
		#done
		# exec
		export XML IF FROMold FROM TOold TO GATEWAY DNS tmpdhcp
		(cd src && ./dhcp.sh)
	}
}

menu
while true
do
case $ANSWER in
	"1.")				
				services
				case $SERVICES in
					"1.")
								vlans
								;;
					"2.")
								dhcp
								;;
					"3.")
								menu
								;;
				esac
				;;
	"2.")
				whiptail --msgbox "Non disponible" 8 18 1
				menu
				;;
	"3.")
				whiptail --msgbox "Non disponible" 8 18 1
				menu
				;;
	"4.")
				break
				;;
esac
done
