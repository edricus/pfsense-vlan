#!/bin/bash
##########
## DHCP ##
##########

# Verification des étendues existantes
if [[ -n $(grep -A6 "<dhcpd>" $XML | grep enable) ]]; then
	whiptail --msgbox "Un serveur DHCP existe déjà. Fermeture." 8 35 1
	exit 1
fi

DNS1=$(echo "$DNS" | cut -d, -f1)
DNS2=$(echo "$DNS" | cut -d, -f2)
# Rédaction du fichier temporaire du DHCP
cat << EOF >> $tmpdhcp
			<enable></enable>
			<failover_peerip></failover_peerip>
			<dhcpleaseinlocaltime></dhcpleaseinlocaltime>
			<defaultleasetime></defaultleasetime>
			<maxleasetime></maxleasetime>
			<netmask></netmask>
			<dnsserver>$DNS1</dnsserver>
			<dnsserver>$DNS2</dnsserver>
			<gateway>$GATEWAY</gateway>
			<domain>$DOMAIN</domain>
			<domainsearchlist></domainsearchlist>
			<denyunknown>class</denyunknown>
			<ddnsdomain></ddnsdomain>
			<ddnsdomainprimary></ddnsdomainprimary>
			<ddnsdomainsecondary></ddnsdomainsecondary>
			<ddnsdomainkeyname></ddnsdomainkeyname>
			<ddnsdomainkeyalgorithm>hmac-md5</ddnsdomainkeyalgorithm>
			<ddnsdomainkey></ddnsdomainkey>
			<mac_allow></mac_allow>
			<mac_deny></mac_deny>
			<ddnsclientupdates>allow</ddnsclientupdates>
			<tftp></tftp>
			<ldap></ldap>
			<nextserver></nextserver>
			<filename></filename>
			<filename32></filename32>
			<filename64></filename64>
			<filename32arm></filename32arm>
			<filename64arm></filename64arm>
			<uefihttpboot></uefihttpboot>
			<rootpath></rootpath>
			<numberoptions></numberoptions>
EOF
# Ajout des clients dans le fichier temporaire
clientnumber=$(ls clients/ | wc -l)
for i in $(seq 1 $clientnumber)
do
	cat << EOF >> $tmpdhcp
			<staticmap>
				<mac>$(grep $STATICMAC clients/client$i)</mac>
				<cid></cid>
				<ipaddr>$(grep $STATICIP clients/client$i)</ipaddr>
				<hostname></hostname>
				<descr></descr>
				<filename></filename>
				<rootpath></rootpath>
				<defaultleasetime></defaultleasetime>
				<maxleasetime></maxleasetime>
				<dnsserver>$DNS1</dnsserver>
				<dnsserver>$DNS2</dnsserver>
				<gateway>$GATEWAY</gateway>
				<domain></domain>
				<domainsearchlist></domainsearchlist>
				<ddnsdomain></ddnsdomain>
				<ddnsdomainprimary></ddnsdomainprimary>
				<ddnsdomainsecondary></ddnsdomainsecondary>
				<ddnsdomainkeyname></ddnsdomainkeyname>
				<ddnsdomainkeyalgorithm>hmac-md5</ddnsdomainkeyalgorithm>
				<ddnsdomainkey></ddnsdomainkey>
				<tftp></tftp>
				<ldap></ldap>
				<nextserver></nextserver>
				<filename32></filename32>
				<filename64></filename64>
				<filename32arm></filename32arm>
				<filename64arm></filename64arm>
				<uefihttpboot></uefihttpboot>
				<numberoptions></numberoptions>
			</staticmap>
EOF
done
# Ecriture du fichier
# Remplacement de la range par defaut par celle des questions
sed "0,/$FROMold/{s/$FROMold/$FROM/g}" -i $XML
sed "0,/$TOold/{s/$TOold/$TO/g}" -i $XML

# Désactiver le DHCP en IPv6
dhcpdv6start=$(grep -Fn '<dhcpdv6>' $XML | cut -d: -f1)
dhcpdv6end=$(grep -Fn '</dhcpdv6>' $XML | cut -d: -f1)
sed -e "${dhcpdv6start},${dhcpdv6end}d" -i $XML

# Ajout de tout les autres paramètres
sed "/<\/range>/ r $tmpdhcp" -i $XML
whiptail --msgbox " Étendue: $FROM - $TO\n Passerelle: $GATEWAY\n DNS 1: $DNS1\n DNS 2: $DNS2\n Le XML a été modifié" 12 45 4
rm -rf $tmpdhcp clients/*
