#!/bin/bash
###########
## VLANS ##
###########
# Verification des VLANs existantes
for i in $( seq $START $INC $END )
do
  if [[ -n $(grep "<tag>$i</tag>" $XML) ]]; then
  	echo "La VLAN $i existe déjà" >> error
  fi
done
if [ -f error ]; then
  whiptail --title "Étendue de VLANs" --backtitle "Configuration des VLANs" \
    --msgbox --scrolltext "$(cat error)" 20 40 8
  echo "Fermeture."
  rm error
  exit 1
fi

# Rédaction du fichier temporaire des VLANs
for i in $( seq $START $INC $END )
do
  cat <<- EOF >> $tmpvlans
  	<vlan>
    	<if>$IF</if>
    	<tag>$i</tag>
    	<pcp>$PCP</pcp>
    	<descr>$DESCR</descr>
    	<vlanif>$IF.$i</vlanif>
    </vlan>
	EOF
done
numberofVLANS=$(grep -Eo '<tag>' $tmpvlans | wc -l)

# Rédaction du fichier temporaire des interfaces
for i in $( seq $START $INC $END )
do
  cat <<- EOF >> $tmpports
  	<opt?>
    	<descr><![CDATA[OPT?]]></descr>
    	<if>$IF.$i</if>
    	<enable></enable>
    	<spoofmac></spoofmac>
  	</opt?>
	EOF
done

# Determiner la dernière interface (variable vide si aucune)
lastOPT=$(grep -Eo '<opt[0-9]{1,4}>' $XML | sed -Ee 's/<|opt|>//g' | tail -n1)
newlastOPT=$((lastOPT+numberofVLANS))

# S'il y a déjà des interfaces (varible non-vide), commencer après la dernière
if [[ -n $lastOPT ]]; then
  for i in $( seq $((lastOPT+1)) $newlastOPT )
  do
    sed "0,/<opt?>/s/<opt?>/<opt$i>/" -i $tmpports
    sed "0,/\[OPT?\]/s/\[OPT?\]/\[OPT$i\]/" -i $tmpports
    sed "0,/<\/opt?>/s/<\/opt?>/<\/opt$i>/" -i $tmpports
  done
else

# Sinon (variable vide), commencer en partant de 1
  for i in $( seq 1 $numberofVLANS )
  do
    sed "0,/<opt?>/s/<opt?>/<opt$i>/" -i $tmpports
    sed "0,/\[OPT?\]/s/\[OPT?\]/\[OPT$i\]/" -i $tmpports
    sed "0,/<\/opt?>/s/<\/opt?>/<\/opt$i>/" -i $tmpports
  done
fi

# Modification des noms des interfaces
numberofOPTS=$(grep -E '<opt[0-9]{1,3}>' $tmpports | wc -l)

if [[ $MANUALOPT == 0 ]]; then
	for i in $(seq 1 $numberofOPTS)
	do
		VLANname=$(grep -A2 "<opt$i>" $tmpports | grep -Eo 'em1.[0-9]{1,6}' | cut -f2 -d.)
		echo "Editing OPT$i, VLAN$VLANname"
		while true
		do
			newnameOPT=$(whiptail --title "Configuration des interfaces" --backtitle "Configuration des VLANs" --inputbox \
			"Nom de la VLAN $VLANname ? (sans espace)" 8 60 "OPT$i" \
			3>&1 1>&2 2>&3
			)
			[[ "$?" = 1 ]] && services
			if [[ -n $(echo $newnameOPT | grep -e \' -e \" -e \& -e ' ' -e \< -e \>) ]]; then
				whiptail --msgbox "Caractères illégaux trouvés\nLes caractères suivants sont interdits:\n\" (double guillemets)\n' (simple guillemet)\n  (espaces)\n& (esperluette)" 13 45
			else break
			fi
		done
		# I = non sensible à la case
		sed "s/opt$i/$newnameOPT/I" -i $tmpports
	done
fi	
#################################
## /!\ Écriture du fichier /!\ ##
#################################
vlanline=$(grep '<vlans>' $XML | cut -f2)
vlanlinenumber=$(grep -nF '<vlans>' $XML | cut -f1 -d:)
if [[ $vlanline == "<vlans></vlans>" ]]; then
	sed -e "${vlanlinenumber}d" -i $XML
	sed "/<\/unbound>/ a\	<vlans>" -i $XML
	sed "/<vlans>/ a\	<\/vlans>" -i $XML
fi

# Comparaison de la première VLAN du fichier avec la dernière qu'on ajoute
# pour respecter l'ordre (petits n° avant les grands)
firstVLAN=$(grep '<tag>' $XML | cut -f4 | sed 's/[^0-9]*//g' | head -n1)
if [[ $END -lt $firstVLAN ]]; then
  sed "/<vlans>/ r $tmpvlans" -i $XML
else
	sed "/<\/vlans>/ e cat $tmpvlans" -i $XML
fi
sed "/<\/interfaces>/ e cat $tmpports" -i $XML
whiptail --msgbox "$numberofVLANS VLANs créées \nLe XML a été modifié" 8 25 1

rm -f $tmpvlans $tmpports

